
import java.util.Date;

import org.omg.CORBA.*;
import org.omg.CosNaming.*;



import org.omg.CORBA.ORBPackage.InvalidName;

public class Client2 {
	
	private static ORB orb = null;
	public static ServerConnection connection = null;

	public static void main(String args[]) {
		java.util.Properties props = System.getProperties();

		int status = 0;

		try {
			orb = ORB.init(args, props);
			run(orb);
		} catch (Exception ex) {
			ex.printStackTrace();
			status = 1;
		}

		if (orb != null) {
			try {
				orb.destroy();
			} catch (Exception ex) {
				ex.printStackTrace();
				status = 1;
			}
		}

		System.exit(status);
	}

	static void run(ORB orb) throws Exception {
		org.omg.CORBA.Object obj = null;
		org.omg.PortableServer.POA rootPOA = org.omg.PortableServer.POAHelper
				.narrow(orb.resolve_initial_references("RootPOA"));
		org.omg.PortableServer.POAManager manager = rootPOA.the_POAManager();
		manager.activate();

		try {

			obj = orb.resolve_initial_references("NameService");
		} catch (InvalidName e) {
			e.printStackTrace();
			System.exit(1);
		}

		NamingContext ctx = NamingContextHelper.narrow(obj);

		if (ctx == null) {
			System.out.println("Le composant NameService n'est pas un repertoire");
			System.exit(1);
		}

		NameComponent[] name = new NameComponent[1];

		name[0] = new NameComponent("Connection", "");

		try {
			obj = ctx.resolve(name);
		} catch (Exception e) {
			System.out.println("Composant inconnu");
			e.printStackTrace();
			System.exit(1);
		}
		
		connection = ServerConnectionHelper.narrow(obj);
		
		Thread t = new ORBRunner(orb);
		t.start();
		
		// Procedure de test pair a pair .
		// ...
		//////////

	}
	
	public static ClientConnection createClientConnection() throws Exception {

		org.omg.CORBA.Object obj;
		org.omg.PortableServer.POA rootPOA = org.omg.PortableServer.POAHelper
				.narrow(orb.resolve_initial_references("RootPOA"));
		
		ClientConnection_Imp clientConnectionImpl = new ClientConnection_Imp();
		obj = rootPOA.servant_to_reference(clientConnectionImpl);
		ClientConnection clientConnection = ClientConnectionHelper.narrow(obj);
		
		return clientConnection;
	}
	
	public static ClientManager createManager() throws Exception {

		org.omg.CORBA.Object obj;
		org.omg.PortableServer.POA rootPOA = org.omg.PortableServer.POAHelper
				.narrow(orb.resolve_initial_references("RootPOA"));
		
		ClientManager_Imp clientManagerImp = new ClientManager_Imp();
		obj = rootPOA.servant_to_reference(clientManagerImp);
		ClientManager clientManager = ClientManagerHelper.narrow(obj);
		
		return clientManager;
	}

}
