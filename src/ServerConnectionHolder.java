
/**
* ServerConnectionHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Pair_A_Pair.idl
* lundi 25 janvier 2016 09 h 34 CET
*/

public final class ServerConnectionHolder implements org.omg.CORBA.portable.Streamable
{
  public ServerConnection value = null;

  public ServerConnectionHolder ()
  {
  }

  public ServerConnectionHolder (ServerConnection initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = ServerConnectionHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    ServerConnectionHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return ServerConnectionHelper.type ();
  }

}
