
import java.util.ArrayList;
import java.util.Iterator;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;

public class Server {
	//ArrayList<Reception> receivers = new ArrayList<Reception>();
	static Server serveur;
	
	public static void main(String[] args) {
		java.util.Properties props = System.getProperties();
		int status = 0;
		ORB orb = null;
		try
		{
			orb = ORB.init(args, props);
			run(orb);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			status = 1;
		}
		if(orb != null)
		{
			try
			{
				orb.destroy();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				status = 1;
			}
		}
		System.exit(status);
	}
	static Server getServer(){
		if(serveur==null){
			serveur = new Server();
		}
		return serveur;
	}
	
	static int run(ORB orb) throws Exception{
		org.omg.CORBA.Object obj;
		org.omg.PortableServer.POA rootPOA =
		org.omg.PortableServer.POAHelper.narrow(
				orb.resolve_initial_references("RootPOA"));
		
	
		org.omg.PortableServer.POAManager manager = rootPOA.the_POAManager();
	
		ServerConnection_Imp connectionImp = new ServerConnection_Imp();
		obj = rootPOA.servant_to_reference(connectionImp);
		
		ServerConnection connection = ServerConnectionHelper.narrow(obj);
		
		obj = orb.resolve_initial_references("NameService");
		NamingContext ctx = NamingContextHelper.narrow(obj);
	
		if (ctx==null)
		{
			System.out.println("Le composant NameService n'est pas un repertoire");
		}
		NameComponent[] name = new NameComponent[1];
		name[0] = new NameComponent("Connection","");
		
		ctx.rebind(name, connection);
		
		System.out.println("Objet cree et reference");
		manager.activate();
		orb.run();

		return 0;
	}
	
}
